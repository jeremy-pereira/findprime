//
//  main.c
//  findprime
//
//  Created by Jeremy Pereira on 16/10/2020.
//

#include <stdio.h>
#include <pthread.h>
#include <math.h>
#include <stdbool.h>
#include "bitset.h"

static long min(long a, long b)
{
	return a < b ? a : b;
}

// We will find primes between 2 and this number inclusive
#define SEARCH_RANGE	10000000
//#define SEARCH_RANGE	100
#define NANO_PER_SEC	1000000000

typedef struct prime_finder_vars
{
	long from;			// First number in this range
	long to;			// First number > `from` not in the range
	long count;			// How many primes have been fond
} PrimeFinderVars;

int is_prime(long num)
{
	long limit = lround(sqrt(num));
	for (long i = 2; i <= limit; i++)
	{
		if (num % i == 0)
			return false;
	}
	return true;
}


void *prime_finder(void *pf)
{

	PrimeFinderVars *pf_vars = (PrimeFinderVars *) pf;

	bool isOdd = (pf_vars->from & 1) != 0;
	long next_cand = pf_vars->from + (isOdd ? 0 : 1);
	while (next_cand < pf_vars->to)
	{
		if (is_prime(next_cand))
		{
			pf_vars->count++ ;
		}
		next_cand += 2;
	}
	return pf;
}


void trial(int numThreads)
{
	struct timespec start;
	struct timespec end;
	double start_sec, end_sec, elapsed_sec;

	clock_gettime(CLOCK_REALTIME, &start);

	pthread_t threads[numThreads];
	PrimeFinderVars vars[numThreads];

	long slotsNeeded = SEARCH_RANGE - 3; 	// We start searching at number 3
	long sum = 1;							// We skipped 2 but it is prime
	// If the number of threads doesn't evenly divide the number of numbers to
	// test, we must round the slice_size up to make sure we have enough slots
	long slice_size = (slotsNeeded + numThreads - 1) / numThreads;

	int currentBase = 3;
	for (int i = 0; i < numThreads; i++)
	{
		vars[i].from = currentBase;
		vars[i].to = min(currentBase + slice_size, SEARCH_RANGE + 1);
		vars[i].count = 0;
		currentBase += slice_size;
		pthread_create(&threads[i], NULL, prime_finder, &vars[i]);

	}

	for (int i = 0; i < numThreads; i++)
	{
		pthread_join(threads[i], NULL);
		sum += vars[i].count;
	}

	clock_gettime(CLOCK_REALTIME, &end);

	start_sec = (double)start.tv_sec + (double)start.tv_nsec / NANO_PER_SEC;
	end_sec = (double)end.tv_sec + (double)end.tv_nsec / NANO_PER_SEC;
	elapsed_sec = end_sec - start_sec;
	printf("%d\t%f\t%ld\n", numThreads, elapsed_sec, sum);
}

void sieve()
{
	BitSet* set = makeBitSet(SEARCH_RANGE + 1);

	struct timespec start;
	clock_gettime(CLOCK_REALTIME, &start);

	if (set == NULL)
	{
		fprintf(stderr, "Failed to create bit set\n");
		return;
	}

	BitSet_invert(set);
	BitSet_set(set, 0, false);
	BitSet_set(set, 1, false);

	long count = 0;

	for (size_t i = 2 ; i < SEARCH_RANGE + 1 ; ++i)
	{
		if (BitSet_isSet(set, i))
		{
			count++;
			for (size_t j = i + i ; j <  SEARCH_RANGE + 1 ; j += i)
			{
				BitSet_set(set, j, false);
			}
		}
	}

	struct timespec end;
	double start_sec, end_sec, elapsed_sec;
	clock_gettime(CLOCK_REALTIME, &end);

	start_sec = (double)start.tv_sec + (double)start.tv_nsec / NANO_PER_SEC;
	end_sec = (double)end.tv_sec + (double)end.tv_nsec / NANO_PER_SEC;
	elapsed_sec = end_sec - start_sec;
	printf("%f\t%ld\n", elapsed_sec, count);

	BitSet_delete(set);
}

int main()
{
	printf("Threads\tTime\tsum\n");
	for (int threads = 1 ; threads <= 50 ; ++threads)
	{
		trial(threads);
	}

	printf("Sieve test");
	sieve();
}
