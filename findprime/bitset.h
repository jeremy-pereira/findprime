//
//  bitset.h
//  findprime
//
//  Created by Jeremy Pereira on 17/10/2020.
//

#ifndef bitset_h
#define bitset_h

#include <stdlib.h>
#include <stdbool.h>

typedef struct BitSet BitSet;

/// Make a bit set of the given size
/// @param size The size of the bitset to create
/// @return A ew bitset
BitSet* makeBitSet(size_t size);

/// Delete and deallocate storage for the bit set
///
/// After calling this function, the pointer no longer points to a valid set.
/// @param set The set to delete
void BitSet_delete(BitSet* set);

/// Invert all the bits in the bit set
/// @param set The set to complement
void BitSet_invert(BitSet* set);

/// Set a bit value to either true or false
///
/// If `bit` is outside the bouds of the set, nothing happens.
/// @param set The set from which to remove the bit
/// @param bit The bit to set
/// @param value true or false
void BitSet_set(BitSet* set, size_t bit, bool value);

/// Return true if a specific bit in the set is set
///
/// If bit is outside the bounds of the set, false is returned.
/// @param set The set to test
/// @param bit The bit within the set
bool BitSet_isSet(BitSet* set, size_t bit);

#endif /* bitset_h */
