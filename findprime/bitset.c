//
//  bitset.c
//  findprime
//
//  Created by Jeremy Pereira on 17/10/2020.
//

#include "bitset.h"
#include <stdint.h>
#include <stdio.h>

#define WORD_BITS	64
#define WORD_COUNT(size) (((size) + WORD_BITS - 1) / WORD_BITS)

struct BitSet
{
	size_t size;
	uint64_t storage[];
};

BitSet* makeBitSet(size_t size)
{
	size_t wordCount = WORD_COUNT(size);
	struct BitSet* ret = malloc(sizeof(struct BitSet) + sizeof(uint64_t) * wordCount);
	if (ret == NULL)
	{
		return NULL;
	}
	ret->size = size;
	for (size_t i = 0 ; i < wordCount ; ++i)
	{
		ret->storage[i] = 0;
	}
	return ret;
}

void BitSet_delete(BitSet* set)
{
	free(set);
}

void BitSet_invert(BitSet* set)
{
	for (size_t i = 0 ; i < WORD_COUNT(set->size) ; ++i)
	{
		set->storage[i] = ~set->storage[i];
	}
}

void BitSet_set(BitSet* set, size_t bit, bool value)
{
	if (bit < set->size)
	{
		size_t wordNumber = bit / WORD_BITS;
		size_t bitNumber = bit % WORD_BITS;
		if (value)
		{
			set->storage[wordNumber] |= 1L << bitNumber;
		}
		else
		{
			set->storage[wordNumber] &= ~(1L << bitNumber);
		}
	}
}

bool BitSet_isSet(BitSet* set, size_t bit)
{
	bool ret = false;
	if (bit < set->size)
	{
		size_t wordNumber = bit / WORD_BITS;
		size_t bitNumber = bit % WORD_BITS;
		ret = (set->storage[wordNumber] & (1L << bitNumber)) != 0;
	}
	return ret;
}
